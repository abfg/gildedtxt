
Founder of CipherMine and Ciphertrade.

Previously known as Robert Craig-Wood.

## Video

* https://www.youtube.com/watch?v=LtohJOUXkYg
* https://www.youtube.com/watch?v=OPMCauFhCOQ
* https://www.youtube.com/watch?v=Z1GbPkAx54U

## Links

* https://bitcointalk.org/index.php?action=profile;u=99792
* http://kate.craig-wood.com/
* http://www.katescomment.com/
* http://en.wikipedia.org/wiki/Kate_Craig-Wood
* http://uk.linkedin.com/in/katecraigwood
* https://twitter.com/Memset_Kate
