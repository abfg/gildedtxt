
Involved with Coinsigner [^1]

Co-founder at CoinMKT.


## Links

* http://www.oladapoajayi.com/
* https://twitter.com/olajayi
* https://angel.co/ola-ajayi-1
* http://www.linkedin.com/in/ajayioladapo
* http://www.meetup.com/Brooklyn-Innovation/members/13676457/
* https://www.liveninja.com/ola.ajayi/

## References

1. http://www.prweb.com/releases/decentralized-bitcoin/exchange-coinsigner/prweb11045517.htm
