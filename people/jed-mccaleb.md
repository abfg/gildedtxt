

Released eDonkey2000 in 2000/09.

Created Mt.Gox in 2011.

Founded new Ripple in 2011.

## Video

* http://www.youtube.com/watch?v=uQ2DCKLHaQs

## Links

* http://en.wikipedia.org/wiki/Jed_McCaleb
* https://bitcointalk.org/index.php?action=profile;u%3D5322

## References

1. http://www.wired.com/wiredenterprise/2013/09/jed_mccaleb/
2. http://techcrunch.com/2014/02/12/jed-mccaleb/
