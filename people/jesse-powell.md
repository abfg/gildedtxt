
Co-founder at Kraken.

# Video

* https://www.youtube.com/watch?v=2M12zzwNkCo
* https://www.youtube.com/watch?v=IJAO2zn0f_s
* https://www.youtube.com/watch?v=r8DV6KL_uZ0
* https://www.youtube.com/watch?v=qQmOTtMVav4
* https://www.youtube.com/watch?v=9uxi6IHDvrU

## Links

* http://jesse.forthewin.com/
* https://bitcointalk.org/index.php?action=profile;u=35541
* https://twitter.com/jespow
* http://www.linkedin.com/in/jessepowell
