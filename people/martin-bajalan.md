
Founder of BTCMarkets.net

Board member at Bitcoin Association of Australia.

## Links

* http://www.linkedin.com/in/martinbajalan
* https://twitter.com/martinbajalan
* http://martinbajalan.wordpress.com/
* https://plus.google.com/109609146425749999071/posts

## References

1. http://www.cse.unsw.com/db/staff/staff_details.php?ID=mbaj471
