
Co-founder at Coinsetter

## Video

* https://www.youtube.com/watch?v=GV-sn5LnPak
* https://www.youtube.com/watch?v=fvFNg6jNqMY
* https://www.youtube.com/watch?v=gahwuAlvUdg
* https://www.youtube.com/watch?v=DfvqNaemSwk
* https://www.youtube.com/watch?v=27-nouuK10s


## Links

* https://bitcointalk.org/index.php?action=profile;u=74396
* https://twitter.com/borntobank
* http://www.linkedin.com/in/jaronlukasiewicz

## References

1. http://upstart.bizjournals.com/money/loot/2013/08/16/subpoenaed-coinsetter-ceo-on-bitcoin.html
