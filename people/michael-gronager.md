
at Kraken.

## Video

* https://www.youtube.com/watch?v=_qdr_Z3hrqQ
* https://www.youtube.com/watch?v=-qMOcFzF2c4

## Links

* https://twitter.com/gronager
* http://dk.linkedin.com/pub/michael-grønager/1/726/448
* https://github.com/gronager
* https://angel.co/gronager
* http://www.meetup.com/Bitcoin-Copenhagen/members/20254061/

## References

1. http://brozkeff.net/2013/10/15/namecoins-dead-as-michael-gronager-publishes-its-fatal-achilles-heel/
