

## Links

* https://bitcointalk.org/index.php?action=profile;u=3
* http://sourceforge.net/u/userid-2321442/
* http://p2pfoundation.ning.com/profile/SatoshiNakamoto

## References


* https://bitcoin.org/bitcoin.pdf
* http://sourceforge.net/p/bitcoin/mailman/bitcoin-list/
* http://p2pfoundation.ning.com/forum/topics/bitcoin-open-source?xg_source=activity
* http://marc.info/?t=122558144600001&r=1&w=2
* http://marc.info/?t=123154968300003&r=1&w=2

