
Owner of mining supply company MegaBigPower.

Picostocks 100TH mine.

## Video

* https://www.youtube.com/watch?v=5CjldZLXiAU

## Links

* https://bitcointalk.org/index.php?action=profile;u=56286

## References

1. http://www.coindesk.com/inside-north-americas-8m-bitcoin-mining-operation/
2. http://www.businessinsider.com/worlds-largest-bitcoin-mining-operation-2014-3
