
Founder at CoinX.

## Video

* https://www.youtube.com/watch?v=G3AgGbyn4io
* https://www.youtube.com/watch?v=hj0S83MUyos
* https://www.youtube.com/watch?v=xJBFaaYGmqQ
* https://www.youtube.com/watch?v=JbIkTqiGZJs

## Links

* http://www.meganburton.com/
* http://www.linkedin.com/in/burtonmegan
* http://www.meetup.com/NY-Bitcoin-Startups/members/90683502/
