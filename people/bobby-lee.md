
Studied/worked at Stanford University. [^1]

Brother of Charles Lee, creator of Litecoin.

Became CEO of BTCChina in April 2013.

## Video

* http://www.youtube.com/watch?v=zz_yfB72S9c
* http://www.youtube.com/watch?v=Piasa1WgHjA
* https://www.youtube.com/watch?v=2M12zzwNkCo

## Links

* http://cn.linkedin.com/in/bobbyclee

## References

1. http://www.youtube.com/watch?v=Piasa1WgHjA
2. http://www.forbes.com/sites/kashmirhill/2013/11/08/from-walmart-to-bitcoin-the-ceo-behind-the-chinese-exchange-sending-bitcoin-to-new-highs/
3. http://www.cryptocoinsnews.com/2014/02/12/future-bitcoin-china-bobby-lee-speaks/
