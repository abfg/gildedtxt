
Creator of OpenTransactions.

Also known as Lovedrop in PUA world. [^1, 2]

## Video

* https://www.youtube.com/watch?v=_qdr_Z3hrqQ
* https://www.youtube.com/watch?v=teNzIFu5L70
* https://www.youtube.com/watch?v=Z9R_98AiLF4
* https://www.youtube.com/watch?v=Y1TSNjFlx14
* https://www.youtube.com/watch?v=HSgpStCTw2g

## Links

* https://github.com/FellowTraveler

## References

1. http://www.venusianarts.com/team/lovedrop/
2. http://www.reddit.com/r/Bitcoin/comments/1fbeqt/i_just_realized_chris_odom_at_the_bitcoin_2013/
