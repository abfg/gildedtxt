
Co-founder of BitX.co.za

## Links

* http://www.timothy.stranex.com/
* https://github.com/tstranex
* http://za.linkedin.com/pub/timothy-stranex/45/954/688
* https://twitter.com/tstranex
* https://plus.google.com/+TimothyStranex/posts
* https://angel.co/tstranex
