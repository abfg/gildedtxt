
Co-founder of Bitstamp.

## Video

* https://www.youtube.com/watch?v=2M12zzwNkCo

## Links

* https://bitcointalk.org/index.php?action=profile;u=38966
* https://twitter.com/nejc_kodric
* http://si.linkedin.com/pub/nejc-kodri%C4%8D/47/227/299
* https://angel.co/nejc-kodric


## References

1. http://www.slovenskenovice.si/novice/slovenija/nejc-damijan-z-virtualnim-bitcoinom-med-bogatase
