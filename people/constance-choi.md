
General counsel for Kraken.

## Video

* http://www.youtube.com/watch?v=T_QwofDM5x4
* https://www.youtube.com/watch?v=_ZNfdbSE2y4
* https://www.youtube.com/watch?v=gvo08OE-ixI
* https://www.youtube.com/watch?v=qQmOTtMVav4

## Links

* http://www.linkedin.com/pub/constance-choi/33/663/54b
* https://angel.co/constance-choi
