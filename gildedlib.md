



Exchange    | ticker | depth | trades | private api | Historical Data
--------------- | ---- | ----------------
bitcoincentral  | yes | yes | yes | no | no
bitfinex        | yes | yes | yes | no | no
bitstamp        | yes | yes | yes | yes | no
bitxf           | yes | yes | yes | no | no
btc100          | yes | yes | yes | no | yes
btcchina        | yes | yes | yes | yes | yes
btce            | yes | yes | yes | yes | no
btcig           | yes | yes | yes | no | no
btcltc          | yes | yes | yes | no | yes
btcmarkets      | yes | yes | yes | no | yes
btcnew          | yes | yes | yes | no | yes
btctrade        | yes | yes | yes | no | yes
btcturk         | yes | yes | yes | no | some
bter            | yes | yes | yes | yes | yes
chbtc           | yes | yes | yes | some | yes
coinedup        | no | yes | yes | yes | no
coinex          | no | yes | yes | no | no
coinse          | yes | yes | yes | some | no
cryptsy         | no | yes | yes | some | no
fxbtc           | yes | yes | yes | no | yes
fyb             | yes | yes | yes | no | yes
kapiton         | yes | yes | yes | no | yes
kraken          | yes | yes | yes | no | some
litetree        | yes | yes | yes | yes | yes
okcoin          | yes | yes | yes | yes | yes
therock         | yes | yes | yes | yes | yes
vaultofsatoshi  | yes | yes | yes | no | no
vircurex        | yes | yes | yes | no | some

campbx          | yes | yes | no | no | no
itbit           | yes | yes | no | no | no
justcoin        | yes | yes | no | no | no

DEAD| History   
----|-----
cryptotrade     | yes  | yes
goxbtc          | yes  | some
masterx         | yes  | yes
mtgox           | yes  | some



