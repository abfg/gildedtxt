List:       cryptography
Subject:    Re: Bitcoin v0.1 released
From:       hal () finney ! org ("Hal Finney")
Date:       2009-01-24 16:48:03
Message-ID: 20090124164803.A3B7414F6E1 () finney ! org
[Download message RAW]

Jonathan Thornburg writes:
> In the modern world, no major government wants to allow untracable
> international financial transactions above some fairly modest size
> thresholds.  (The usual catch-phrases are things like "laundering
> drug money", "tax evasion", and/or "financing terrorist groups".)
> To this end, electronic financial transactions are currently monitored
> by various governments & their agencies, and any but the smallest of
> transactions now come with various ID requirements for the humans
> on each end.
>
> But if each machine in a million-node botnet sends 10 cents to a
> randomly chosen machine in another botnet on the other side of the
> world, you've just moved $100K, in a way that seems very hard to
> trace.  To me, this means that no major government is likely to allow
> Bitcoin in its present form to operate on a large scale.

Certainly a valid point, and one which has been widely discussed in
the debates over the years about electronic cash. Bitcoin has a couple
of things going for it: one is that it is distributed, with no single
point of failure, no "mint", no company with officers that can be
subpoenaed and arrested and shut down. It is more like a P2P network,
and as we have seen, despite degrees of at least governmental distaste,
those are still around.

Bitcoin could also conceivably operate in a less anonymous mode, with
transfers being linked to individuals, rather than single-use keys. It
would still be useful to have a large scale, decentralized electronic
payment system.

It also might be possible to refactor and restructure Bitcoin to separate
out the key new idea, a decentralized, global, irreversible transaction
database. Such a functionality might be useful for other purposes. Once
it exists, using it to record monetary transfers would be a sort of side
effect and might be harder to shut down.

> I also worry about other "domestic" ways nasty people could exploit
> a widespread Bitcoin deployment:
> * Spammer botnets could burn through pay-per-send email filters
>   trivially (as usual, the costs would fall on people other than the
>   botnet herders & spammers).
> * If each machine in a botnet sends 1 cent to a herder, that can add
>   up to a significant amount of money.  In other words, Bitcoin would
>   make botnet herding and the assorted malware industry even more
>   profitable than it already is.

It's important to understand that the proof-of-work (POW) aspect of
Bitcoin is primarily oriented around ensuring the soundness of the
historical transaction database. Each Bitcoin data block records a set
of transactions, and includes a hash collision. Subsequent data blocks
have their own transactions, their own collisions, and also chain to
all earlier hashes.  The result is that once a block is "buried" under
enough new blocks, it is essentially certain (given the threat model,
namely that attackers cannot muster more than X% of the compute power
of legitimate node operators) that old transactions can't be reversed.

Creating new coins is indeed currently also being done by POW, but I
think that is seen as a temporary expedient, and in fact the current
software phases that out over several years. Hence worries about botnets
being able to manufacture large quantities of POW tokens are only a
temporary concern, in the context of Bitcoin.

There have been a number of discussions in the past about POW tokens as
anti spam measures, given the botnet threat. References are available from
"Proof-of-work system" on Wikipedia. Analyses have yielded mixed results,
depending on the assumptions and system design.

If POW tokens do become useful, and especially if they become money,
machines will no longer sit idle. Users will expect their computers to
be earning them money (assuming the reward is greater than the cost to
operate). A computer whose earnings are being stolen by a botnet will
be more noticeable to its owner than is the case today, hence we might
expect that in that world, users will work harder to maintain their
computers and clean them of botnet infestations.

Countermeasures by botnet operators would include moderating their take,
perhaps only stealing 10% of the productive capacity of invaded computers,
so that their owners would be unlikely to notice. This kind of thinking
quickly degenerates into unreliable speculation, but it points out the
difficulties of analyzing the full ramifications of a world where POW
tokens are valuble.

Hal Finney

---------------------------------------------------------------------
The Cryptography Mailing List
Unsubscribe by sending "unsubscribe cryptography" to majordomo@metzdowd.com
