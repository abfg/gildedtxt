List:       cryptography
Subject:    Re: Bitcoin P2P e-cash paper
From:       "Satoshi Nakamoto" <satoshi () vistomail ! com>
Date:       2008-11-06 20:15:40
Message-ID: CHILKAT-MID-8342fe26-f3e0-089a-237e-6fa56bec088c () server123
[Download message RAW]

> [Lengthy exposition of vulnerability of a systm to use-of-force
> monopolies ellided.]
> 
> You will not find a solution to political problems in cryptography.

Yes, but we can win a major battle in the arms race and gain a new territory of \
freedom for several years.

Governments are good at cutting off the heads of a centrally controlled networks like \
Napster, but pure P2P networks like Gnutella and Tor seem to be holding their own.  

Satoshi


---------------------------------------------------------------------
The Cryptography Mailing List
Unsubscribe by sending "unsubscribe cryptography" to majordomo@metzdowd.com
