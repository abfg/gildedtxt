List:       cryptography
Subject:    Re: Bitcoin P2P e-cash paper
From:       "James A. Donald" <jamesd () echeque ! com>
Date:       2008-11-02 23:46:23
Message-ID: 490E3BCF.2020109 () echeque ! com
[Download message RAW]

Satoshi Nakamoto wrote:
> I've been working on a new electronic cash system that's fully
> peer-to-peer, with no trusted third party.
> 
> The paper is available at:
> http://www.bitcoin.org/bitcoin.pdf

We very, very much need such a system, but the way I understand your 
proposal, it does not seem to scale to the required size.

For transferable proof of work tokens to have value, they must have 
monetary value.  To have monetary value, they must be transferred within 
a very large network - for example a file trading network akin to 
bittorrent.

To detect and reject a double spending event in a timely manner, one 
must have most past transactions of the coins in the transaction, which, 
  naively implemented, requires each peer to have most past 
transactions, or most past transactions that occurred recently. If 
hundreds of millions of people are doing transactions, that is a lot of 
bandwidth - each must know all, or a substantial part thereof.

---------------------------------------------------------------------
The Cryptography Mailing List
Unsubscribe by sending "unsubscribe cryptography" to majordomo@metzdowd.com
