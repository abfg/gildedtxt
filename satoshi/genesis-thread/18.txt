List:       cryptography
Subject:    Re: Bitcoin P2P e-cash paper
From:       "Satoshi Nakamoto" <satoshi () vistomail ! com>
Date:       2008-11-10 2:14:30
Message-ID: CHILKAT-MID-41c54a12-e2ac-4042-255f-1fcda7a4a00b () server123
[Download message RAW]

James A. Donald wrote:
> Furthermore, it cannot be made to work, as in the
> proposed system the work of tracking who owns what coins
> is paid for by seigniorage, which requires inflation.

If you're having trouble with the inflation issue, it's easy to tweak it for \
transaction fees instead.  It's as simple as this: let the output value from any \
transaction be 1 cent less than the input value.  Either the client software \
automatically writes transactions for 1 cent more than the intended payment value, or \
it could come out of the payee's side.  The incentive value when a node finds a \
proof-of-work for a block could be the total of the fees in the block.

Satoshi Nakamoto


---------------------------------------------------------------------
The Cryptography Mailing List
Unsubscribe by sending "unsubscribe cryptography" to majordomo@metzdowd.com
