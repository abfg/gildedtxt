[gambling]
name = "Just Dice"
url = "https://just-dice.com/"
founded = "2013"
country = "Canada"
---

## History

* 2013/06/20 - announced on bitcointalk [^1,2]
* 2013/12/07 - doge-dice announced [^3]

## Associated names

* Chris Moore (dooglus https://bitcointalk.org/index.php?action=profile;u=3420)

## Links

* http://just-dice.blogspot.ca/
* https://twitter.com/JustDiceSupport
* http://whois.domaintools.com/just-dice.com

## References

1. https://bitcointalk.org/index.php?topic=238613
2. https://bitcointalk.org/index.php?topic=242962
3. https://bitcointalk.org/index.php?topic=374932

