[gambling]
name = "Bitcoin Video Casino"
url = "https://bitcoinvideocasino.com/"
founded = "2012"
country = "-"
---

## History

* 2012/12/09 - announced on bitcointalk [^1,2]
* 2013/10/01 - hacking attempt prevented [^3]
* 2014/01/20 - website/business for sale [^4]

## Associated names

* (BitcoinVideoPoker https://bitcointalk.org/index.php?action=profile;u=74635)

## Links

* https://twitter.com/BTCVideoCasino
* http://whois.domaintools.com/bitcoinvideocasino.com
* http://whois.domaintools.com/bitcoinvideopoker.com

## References

1. https://bitcointalk.org/index.php?topic=129901
2. https://bitcointalk.org/index.php?topic=130242
2. https://bitcointalk.org/index.php?topic=305780
3. https://bitcointalk.org/index.php?topic=424317
