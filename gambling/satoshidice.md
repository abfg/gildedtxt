[gambling]
name = "SatoshiDice"
url = "http://www.satoshidice.org/"
founded = "2012"
country = "-"
---

## History

* 2012/04/24 - announced on bitcointalk [^1]
* 2013/07/18 - sold to a private party [^2]

## Associated names

* Erik Tristan Voorhees (evoorhees https://bitcointalk.org/index.php?action=profile;u=12149)


## Links

* http://en.wikipedia.org/wiki/SatoshiDice
* http://whois.domaintools.com/satoshidice.org

## References

1. https://bitcointalk.org/?topic=77870
2. https://bitcointalk.org/index.php?topic=101902.0

