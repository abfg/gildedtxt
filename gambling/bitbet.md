[gambling]
name = "BitBet"
url = "http://bitbet.us/"
founded = "2013"
country = "-"
---

## History

* 2013/01/05 - announced on bitcointalk [^1]
* 2013/01/05 - listed on MPEx [^2]

## Associated names

* Matic Kočevar (kakobrekla https://bitcointalk.org/index.php?action=profile;u=11998)
* Mircea Popescu

## Links

* http://whois.domaintools.com/bitbet.us

## References

1. https://bitcointalk.org/index.php?topic=134799.0
2. http://mpex.co/?mpsic=S.BBET
