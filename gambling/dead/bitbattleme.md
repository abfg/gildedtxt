[gambling]
name = "BitBattle.me"
url = "http://bitbattle.me/"
founded = "2012"
country = "-"
---

## History

* 2012/10/25 - announced on bitcointalk [^1]
* 2013/11/19 - hacked, no funds lost [^2]

## Associated names

* Michael Bauer (Herbert https://bitcointalk.org/index.php?action=profile;u=37905)


## Links

* http://whois.domaintools.com/primedice.com

## References

1. https://bitcointalk.org/index.php?topic=120497.0
2. https://bitcointalk.org/index.php?topic=120497.msg3640291#msg3640291

