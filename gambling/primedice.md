[gambling]
name = "PrimeDice"
url = "https://primedice.com/"
founded = "2013"
country = "-"
---

## History

* 2013/05/18 - announced on bitcointalk [^1]

## Associated names

* (Stunna https://bitcointalk.org/index.php?action=profile;u=81292)


## Links

* http://blog.primedice.com/
* https://twitter.com/primedice
* http://whois.domaintools.com/primedice.com

## References

1. https://bitcointalk.org/index.php?topic=208986.0

