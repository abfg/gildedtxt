[exchange]
name = "BTC-e"
url = "https://btc-e.com/"
founded = "2011"
country = "Bulgaria"
---

## History

* 2011/07/17 - launch announced on bitcointalk [^6]
* 2013/08/20 - MetaTrader 4 integration introduced [^1]

Invented the trollbox.

"For all legal purposes, these Terms of Use shall be governed by the laws applicable in the Cyprus." [^2]

"the service is based in Bulgaria, one of the banks it uses is located in the Czech Republic, and its managing company is based in Cyprus." [^4]

## Associated names

* (btc-e.com https://bitcointalk.org/index.php?action=profile;u=33012)

Payment processor:

* Nikolay Rozhok
* Mayzus Financial Services Ltd
* Mayzus Investment Company Ltd
* UWC Financial Services Ltd

## API

* https://btc-e.com/api/documentation
* https://btc-e.com/page/2


## Links

* https://bitcointalk.org/index.php?action=profile;u=33012
* http://en.wikipedia.org/wiki/BTC-E
* http://whois.domaintools.com/btc-e.com

## References

1. https://btc-e.com/news/172
2. https://btc-e.com/page/1 
3. http://www.coindesk.com/russian-prosecutors-office-btc-e-investigation-hoax/
4. http://www.coindesk.com/btc-e-recent-issues-caused-surge-users/
5. http://www.reddit.com/r/Bitcoin/comments/1rf0bh/warning_btce_selectively_scamming/
6. https://bitcointalk.org/index.php?topic=29698.msg373031
7. http://www.coindesk.com/btc-e-exchange-banking-issues/
