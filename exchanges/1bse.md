[exchange]
name = "1bse"
url = "https://www.1bse.com/"
founded = "2013"
country = "Hong Kong"
---

## History

* 2013/12/20 - open for trading [^1]

## Associated names

* Alex Yanchenko

* World of Start Ltd.

## API


## Links

* https://twitter.com/1Bsecom
* https://localbitcoins.com/accounts/profile/1Bse.com/
* http://whois.domaintools.com/1bse.com

## References

1. https://www.1bse.com/node/49

