[exchange]
name = "Bter"
url = "http://bter.com/"
founded = "2013"
country = "China"
---

## History

* 2013/08/25 - Bter announced on bitcointalk [^1]

* 2014/01/12 - Bter Trader trading software released [^2]


## Associated names

* freeworm

## API

* http://bter.com/api

trades: since id, full history

## Links

* http://whois.domaintools.com/bter.com

## References

1. https://bitcointalk.org/index.php?topic=280806.0
2. http://bter.com/article/11

