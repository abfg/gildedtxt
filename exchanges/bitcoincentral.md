[exchange]
name = "Bitcoin-Central"
url = "https://bitcoin-central.net/"
founded = "2011"
country = "France"
---

## History

* 2010/12/29 - launch announced on bitcointalk [^1]
* 2012/12/06 - partnership with Aqoba announced [^2]
* 2013/04/24 - hacked twice, a few hundred BTC stolen [^3,4]

## Associated names

* David Francois (davout https://bitcointalk.org/index.php?action=profile;u=1929)
* Gonzague Grandval (http://fr.linkedin.com/pub/gonzague-grandval/3/8b9/694)
* Pierre Noizat  (http://fr.linkedin.com/pub/pierre-noizat/13/24a/878)

* Stephan Florquin
* Nicolas Papon

* Paymium, SAS

## API

* https://github.com/Paymium/api-documentation

trades: no since id (yet)

## Links

* http://www.paymium.com/
* https://twitter.com/paymium
* http://whois.domaintools.com/bitcoin-central.net

## References

1. https://bitcointalk.org/index.php?topic=2519.0
2. https://bitcointalk.org/index.php?topic=129461.0
3. https://twitter.com/bitcoin_central/status/327131323342942209
4. https://bitcointalk.org/index.php?topic=186609.0

