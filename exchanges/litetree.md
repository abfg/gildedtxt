[exchange]
name = "Litetree"
url = "https://litetree.com/"
founded = "2013"
country = "US"
---

## History

* 2013/07/22 - announced on litecointalk [^3]

## Associated names

* Jonathan Speigner
* Amit Orgad
* (Engine-IT https://bitcointalk.org/index.php?action=profile;u=155113)

* Litetree LLC

## API

* https://www.litetree.com/api

trades: since id, full history

## Links

* https://twitter.com/litetree
* http://whois.domaintools.com/litetree.com

## References

1. https://bitcointalk.org/index.php?topic=20292.msg3441029#msg3441029
2. http://www.crunchbase.com/company/litetree
3. https://litecointalk.org/index.php?topic=5164.0
