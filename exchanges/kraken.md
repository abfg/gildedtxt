[exchange]
name = "Kraken"
url = "https://www.kraken.com/"
founded = "2013"
country = "US"
---

## History


* 2012/10/07 - fundraising announcement on bitcointalk [^4]
* 2013/05/03 - open beta announced [^2]


## Associated names

* Jesse Powell
* Constance Choi
* Walter Stanish
* Michael Grønager
* Ayako Miyaguchi
* Elliot Lee
* Dargo (https://bitcointalk.org/index.php?action=profile;u=32856)

* Payward, Inc. [^1]


## API

* https://www.kraken.com/help/api

trades: since id?, no trade ids, no history

## Links

* https://github.com/payward
* https://twitter.com/krakenfx
* https://www.facebook.com/KrakenFX
* http://en.wikipedia.org/wiki/Kraken_%28digital_currency_exchange%29
* http://whois.domaintools.com/kraken.com

## References

1. https://www.kraken.com/about/payward
2. https://twitter.com/krakenfx/status/330232680711073793
3. https://angel.co/payward-kraken
4. https://bitcointalk.org/index.php?topic=116955.0

