[exchange]
name = "BTC Markets"
url = "https://www.btcmarkets.net/"
founded = "2013"
country = "Australia"
---

## History

* 2013/09/13 - launch announced on bitcointalk [^1]

## Associated names

* Martin Bajalan
* (BTCMarkets https://bitcointalk.org/index.php?action=profile;u=151158)

* BTC Markets Pty Ltd

## API

* https://github.com/BTCMarkets/API

## Links

* https://twitter.com/btcmarkets
* http://whois.domaintools.com/btcmarkets.net

## References

1. https://bitcointalk.org/index.php?topic=297142.msg3187855
2. http://www.coindesk.com/australia-gets-new-bitcoin-exchange-btc-markets/
3. http://bitcoinexaminer.org/meet-btc-markets-australias-new-bitcoin-and-litecoin-exchange-platform/
