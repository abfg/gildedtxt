[exchange]
name = "mcxNOW"
url = "https://mcxnow.com/"
founded = "2013"
country = "-"
[api]
streaming = 0
history = 0
trade = 1
---

## History

* 2013/04/28 - open for trading [^1]
* 2013/09/12 - announced on bitcointalk [^2]
* 2013/11/01 - final investment round [^3]
* 2013/11/27 - trading suspended [^4,5]

## Associated names

* (TheRealSolid https://bitcointalk.org/index.php?action=profile;u=102245)
* (CoinHunter https://bitcointalk.org/index.php?action=profile;u=34967)

## API

* https://mcxnow.com/api.html

## Links

* http://whois.domaintools.com/mcxnow.com

## References

1. https://mcxnow.com/
2. https://bitcointalk.org/index.php?topic=293035.0
3. https://bitcointalk.org/index.php?topic=322656
4. http://www.coindesk.com/mcxnow-cryptoexchange-suspends-trading/
5. https://bitcointalk.org/index.php?topic=346890.0

