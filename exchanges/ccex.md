[exchange]
name = "C-CEX"
url = "https://c-cex.com/"
founded = "2014"
country = "Ukraine"
---

## History

* 2014/01/16 - announced on bitcointalk [^1]


## Associated names

* (c-cex https://bitcointalk.org/index.php?action=profile;u=220467)

## API

* https://c-cex.com/index.html?id=api

trades: since id, full history

## Links

* https://twitter.com/CryptoCurrEncyX
* http://whois.domaintools.com/c-cex.com

## References

1. https://bitcointalk.org/index.php?topic=418700.0

