[exchange]
name = "BitX"
url = "https://bitx.co.za/"
founded = "2013"
country = "South Africa"
---

## History

* 2013/10/21 - acquired by Switchless [^1]

## Associated names

* Timothy Stranex
* Nicholas Pilkington 

## API

* https://bitx.co.za/api

## Links

* https://twitter.com/bitx_za
* http://whois.domaintools.com/bitx.co.za

## References

1. http://www.coindesk.com/software-firm-buys-africas-largest-bitcoin-exchange/
2. http://www.humanipo.com/news/5649/sas-first-bitcoin-exchange-expecting-rapid-growth/
