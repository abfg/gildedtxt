[exchange]
name = "SwissCEX"
url = "https://www.swisscex.com/"
founded = "2014"
country = "Switzerland"
---

## History

* 2014/02/01 - announced on bitcointalk [^1]

## Associated names

* (Swisscex https://bitcointalk.org/index.php?action=profile;u=233974)

## API

* https://www.swisscex.com/docs/api

## Links

* https://twitter.com/swisscex
* http://whois.domaintools.com/swisscex.com

## References

1. https://bitcointalk.org/index.php?topic=443205.0
2. https://bitcointalk.org/index.php?topic=460689.0

