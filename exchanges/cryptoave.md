[exchange]
name = "CryptoAve"
url = "https://cryptoave.com/"
founded = "2013"
country = "-"
---

## History

* 2013/08/18 - announced on bitcointalk [^1]
* 2013/10/10 - selling shares, final round [^2]
* 2014/03/16 - preemptive shutdown [^4]

## Associated names

* (baritus https://bitcointalk.org/index.php?action=profile;u=89750)

## API


## Links

* https://twitter.com/CryptoAve
* http://whois.domaintools.com/cryptoave.com

## References

1. https://bitcointalk.org/index.php?topic=277241.0
2. http://digitalcoin.co/forums/index.php/topic,186
3. http://www.followthecoin.com/speak-baritus-digitalcoin-securecoin-argentum-crypto-ave/
4. http://digitalcoin.co/forums/index.php?topic=951.0

