[exchange]
name = "CoinMKT"
url = "https://coinmkt.com/"
founded = "2013"
country = "US"
[api]
streaming = 0
history = 0
trade = 0
---

## History

* 2013/05/24 - announced on bitcointalk [^1]
* 2013/09/07 - public beta announced [^2]

## Associated names

* Travis Skweres (tskweres https://bitcointalk.org/index.php?action=profile;u=119655)
* Oladapo Ajayi

* West Orange Labs, Inc

## API

* https://coinmkt.com/#!api

## Links

* https://twitter.com/coinmkt
* http://whois.domaintools.com/coinmkt.com

## References

1. https://bitcointalk.org/index.php?topic=215352.msg2259012
2. http://blog.coinmkt.com/coin-market-public-beta-is-live/
3. https://bitcointalk.org/index.php?topic=283384
