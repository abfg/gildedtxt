[exchange]
name = "Pmtocoins"
url = "https://pmtocoins.com/"
founded = "2014"
country = "India"
---

## History

* 2014/01/27 - selling shares [^2]
* 2014/02/04 - announced on bitcointalk [^1]

## Associated names

* (Shahrukh https://bitcointalk.org/index.php?action=profile;u=158411)

* AD Giant Media Group LLC

## API


## Links

* https://twitter.com/pmtocoins
* http://whois.domaintools.com/pmtocoins.com

## References

1. https://bitcointalk.org/index.php?topic=447858.0
2. https://bitcointalk.org/index.php?topic=434967.msg4778214#msg4778214

