[exchange]
name = "ANX"
url = "https://anxbtc.com/"
founded = "2013"
country = "Hong Kong"
---

## History

* 2013/07/25 - announced on bitcointalk [^1]
* 2014/01/21 - ANXPRO announced [^2]

## Associated names

* Ken Lo
* (AsiaNexgen https://bitcointalk.org/index.php?action=profile;u=137981)


## API

* http://docs.anxv2.apiary.io/

## Links

* https://anxbtc.com/
* https://anxpro.com/
* https://twitter.com/AsiaBitcoin
* http://whois.domaintools.com/anxbtc.com
* http://whois.domaintools.com/anxpro.com

## References

1. https://bitcointalk.org/index.php?topic=262091
2. https://bitcointalk.org/index.php?topic=425451
3. http://edition.cnn.com/video/data/2.0/video/business/2014/03/03/intv-stevens-ken-lo-bitcoin.cnn.html

