[exchange]
name = "HitBTC"
url = "https://hitbtc.com/"
founded = "2013"
country = "-"
---

## History

* 2013/12/20 - announced on bitcointalk [^1]

```
hitbtc.com was founded in 2013 by a group of British investors specialized in high-tech start-up. We have raised over 1 million USD in its first round of financing, held in January 2013. In November 2013 a second round of financing was held and generated 6 million USD. hitbtc.com is licensed by the Ullus Corporation to use their technology. A third round is planned for summer 2014. [^2]
```


## Associated names

* (hitbtc https://bitcointalk.org/index.php?action=profile;u=194708)

* Ullus Corporation

## API

* http://hitbtc-com.github.io/hitbtc-api/

## Links

* https://twitter.com/hitbtc
* http://whois.domaintools.com/hitbtc.com

## References

1. https://bitcointalk.org/index.php?topic=378827.0
2. https://hitbtc.com/about-us
3. http://www.reddit.com/r/Bitcoin/comments/1y1ig3/bitcoin_exchange_hitbtc_launches_february_14th/cfgqm1s
4. https://bitcointalk.org/index.php?topic=484106.0
