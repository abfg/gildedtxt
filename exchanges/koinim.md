[exchange]
name = "Koinim"
url = "https://koinim.com/"
founded = "2013"
country = "Turkey"
---

## History

2013/09/18 - announced on bitcointalk [^1]

## Associated names

* Çağatay Yüksel
* Merve Yüksel

## API


## Links

* https://bitcointalk.org/index.php?action=profile;u=150698
* https://twitter.com/koinimcom
* http://whois.domaintools.com/koinim.com

## References

1. https://bitcointalk.org/index.php?topic=296397
