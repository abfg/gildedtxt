[exchange]
name = "JustCoin"
url = "https://justcoin.com/"
founded = "2013"
country = "Norway"
---

## History

* 2013/4/12 - announced on bitcointalk [^1]

## Associated names

* Andreas Brekken 
* Klaus Bugge Lund

* Justcoin AS

## API

* https://github.com/justcoin/snow/blob/master/docs/calls.md


## Links

* https://twitter.com/jstcoin
* http://whois.domaintools.com/justcoin.com

## References

1. https://bitcointalk.org/index.php?topic=174643.0
2. https://justcoin.com/en/about
3. http://www.proff.no/roller/justcoin-as/oslo/-/IF2Y3PC0000/
