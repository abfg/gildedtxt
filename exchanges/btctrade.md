[exchange]
name = "BTCTrade"
url = "https://btctrade.com/"
founded = "2013"
country = "China"
---

## History

* 2013/06/29 - announced on bitcointalk [^1]

## Associated names

* (BtcTrade https://bitcointalk.org/index.php?action=profile;u=130944)

## API

trades: since id, full history

## Links

* http://www.btcbbs.com/home.php?mod=space&uid=8548
* http://whois.domaintools.com/btctrade.com

## References

1. https://bitcointalk.org/index.php?topic=246314.0
