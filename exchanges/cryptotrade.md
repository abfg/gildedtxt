[exchange]
name = "Crypto-trade"
url = "https://crypto-trade.com/"
founded = "2013"
country = "Hong Kong"
---

## History

* 2013/03/05 - announced IPO on BTCT and LTC-Global [^1]
* 2013/05/26 - site open for trading [^3]

Some securities traded.

Related to Koddos.com

## Associated names

* (neotrix https://bitcointalk.org/index.php?action=profile;u=61853)

* Esecurity SA, registered in Belize [^2]

## API

* https://crypto-trade.com/api/documentation

trades: since id, full history

## Links

* https://twitter.com/Crypto_Trade
* http://whois.domaintools.com/crypto-trade.com

## References

1. https://bitcointalk.org/index.php?topic=149458.0
2. https://bitcointalk.org/index.php?topic=149458.msg1588286#msg1588286
3. https://bitcointalk.org/index.php?topic=149458.msg2274709#msg2274709

