[exchange]
name = "OKCoin"
url = "https://okcoin.com/"
founded = "2013"
country = "China"
---

## History

* 2013/03/14 - raised $10m in Series A funding from  Ceyuan,Mandra Capital, VenturesLab  [^3]


## Associated names

* Xu Mingxing
* Star Xu (http://www.crunchbase.com/person/star-xu)
* Jennifer Chen (http://cn.linkedin.com/pub/jennifer-chen/66/7a6/278)

## API

* https://www.okcoin.com/t-1000052.html
* https://www.okcoin.com/t-1000097.html

trades: since id, full history

## Links

* https://bitcointalk.org/index.php?action=profile;u%3D126236
* http://www.weibo.com/okcoin
* http://whois.domaintools.com/okcoin.com

## References

1. http://www.coindesk.com/chinese-bitcoin-exchange-okcoin-accused-faking-trading-data/
2. http://www.reddit.com/r/Bitcoin/comments/1t2iay/okcoincom_present_situation_in_china_to_clear/
3. http://www.finsmes.com/2014/03/okcoin-raises-10m-in-series-a-financing.html
