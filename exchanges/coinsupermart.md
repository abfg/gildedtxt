[exchange]
name = "CoinSupermart"
url = "https://www.coinsupermart.com/"
founded = "2013"
country = "Singapore"
---

## History

* 2014/01/06 - launch [^1]

## Associated names

* Gerry Eng
* Yusho Liu

* Coin Supermart Pte. Ltd.

## API


## Links

* https://twitter.com/coinsupermart
* https://www.facebook.com/CoinSupermart
* https://angel.co/coin-supermart
* http://whois.domaintools.com/coinsupermart.com

## References

1. https://twitter.com/coinsupermart/status/420146095428079616
