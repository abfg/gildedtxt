[exchange]
name = "itBit"
url = "https://www.itbit.com/"
founded = "2013"
country = "Singapore"
---

## History

* 2013/11/14 - launch announced on bitcointalk [^2]

## Associated names

* Richmond Teo
* Jason Melo
* J. Scott Miller
* Wee Horng Ang
* John Zhu
* Jesse Pakin
* Darren Ng
* Antony Lewis

* itBit PTE. Ltd

## API

* http://docs.itbit.apiary.io/

## Links

* https://bitcointalk.org/index.php?action=profile;u=60059
* https://twitter.com/itBit
* http://whois.domaintools.com/itbit.com

## References

1. https://www.itbit.com/p/team
2. https://bitcointalk.org/index.php?topic=20292.msg3577267#msg3577267
3. http://bitcoinmagazine.com/8153/itbit-to-launch-as-a-global-bitcoin-currency-exchange/

