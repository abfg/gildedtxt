[exchange]
name = "The Rock Trading"
url = "https://therocktrading.com/"
founded = "2007"
country = "Malta"
---

## History

* 2011/11/15 - announced on bitcointalk [^1]

"Born in 2007 as a Second Life virtual company, The Rock has evolved in a full featured digital institution incorporated in Malta."

Also trades stocks/securities.

## Associated names

* Andrea Medri
* Davide Barbieri

* The Rock Trading Ltd

## API

* https://www.therocktrading.com/en/pages/api

trades: since id, full history

## Links

* https://twitter.com/TheRockTrading
* http://whois.domaintools.com/therocktrading.com

## References

1. https://bitcointalk.org/index.php?topic=330856.0
