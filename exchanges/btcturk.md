[exchange]
name = "BTCTurk"
url = "hhttps://btcturk.com/"
founded = "2013"
country = "Turkey"
---

## History



## Associated names

* Emre Kenci
* Kerem Tibuk
* Ozan Yurtseven 

* Bitcoin Trading Ltd

## API

* https://www.btcturk.com/yardim/api-home-page

trades: since id

## Links

* https://bitcointalk.org/index.php?action=profile;u=134481
* https://twitter.com/btcturk
* http://whois.domaintools.com/btcturk.com

## References

1. http://www.coindesk.com/btcturk-becomes-the-first-turkish-lira-to-bitcoin-exchange/
