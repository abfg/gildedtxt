[exchange]
name = "Coinnext"
url = "http://www.coinnext.com/"
founded = "2014"
country = "Netherlands"
---

## History

* 2014/02/04 - announced on bitcointalk [^1]

## Associated names

* Sebastian Mortelmans
* Charles De Meulenaer
* Dumitru Glavan

* (coinnext https://bitcointalk.org/index.php?action=profile;u=172885)

## API


## Links

* https://twitter.com/coinnext
* http://whois.domaintools.com/coinnext.com

## References

1. https://bitcointalk.org/index.php?topic=448612

