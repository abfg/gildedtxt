[exchange]
name = "CoinX"
url = "https://www.coinxtrader.com/"
founded = "2013"
country = "US"
---

## History



## Associated names

* Megan Burton
* Sherwin Krug (http://www.linkedin.com/in/sherwinkrug)
* Roseanne Lazer (http://www.linkedin.com/in/rclazer)
* Erik Cohen
* Joe Cammarata

* CoinX, Inc

## API


## Links

* https://twitter.com/coinxinc
* https://www.facebook.com/coinxinc
* https://www.coinx.com/
* http://whois.domaintools.com/coinxtrader.com

## References

1. http://www.prweb.com/releases/2013/10/prweb11208154.htm
2. http://www.coindesk.com/bitcoin-exchange-coinx-state/
