[exchange]
name = "Coins-e"
url = "https://coins-e.com/"
founded = "2013"
country = ""
---

## History

* 2013/06/22 - launch announced on twitter [^6]
* 2013/08/07 - introduction on bitcointalk [^1]

related to OrbitCoin? [^2]

## Associated names

* (Coins-e https://bitcointalk.org/index.php?action=profile;u=143456)

* Inspirin [^5]

## API

* https://www.coins-e.com/exchange/api/documentation/

trades: no since id, no history

## Links

* https://twitter.com/coins_e
* http://whois.domaintools.com/coins-e.com

## References

1. https://bitcointalk.org/index.php?topic=269827.msg2887044#msg2887044
2. https://bitcointalk.org/index.php?topic=270188.msg2891908#msg2891908
3. https://bitcointalk.org/index.php?topic=418416.0
4. https://bitcointalk.org/index.php?topic=294224.0
5. https://www.coins-e.com/tos/
6. https://twitter.com/coins_e/status/348466797193859072
