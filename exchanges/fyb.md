[exchange]
name = "FYB-SG"
url = "https://www.fybsg.com/"
founded = "2013"
country = "Singapore"
---

## History

* 2012/12/19 - FYB-SG seeking beta testers [^1]
* 2013/01/02 - FYB-SG announced on bitcointalk [^2]
* 2013/07/01 - FYB-SE launched
* 2013/09/02 - FYB-SE announced on bitcointalk [^3]

## Associated names

* (Nagato https://bitcointalk.org/index.php?action=profile;u=75543)
* (KSV https://bitcointalk.org/index.php?action=profile;u=92353)

## API

* http://docs.fyb.apiary.io/

## Links

* https://www.fybsg.com/
* https://www.fybse.se/

* https://twitter.com/FYBSG
* https://twitter.com/FYBSE
* http://whois.domaintools.com/fybsg.com
* http://whois.domaintools.com/fybse.se

## References

1. https://bitcointalk.org/index.php?topic=131604.msg1409186
2. https://bitcointalk.org/index.php?topic=134340.0
3. https://bitcointalk.org/index.php?topic=286649.0

