[exchange]
name = "OpenEX"
url = "https://openex.pw/"
founded = "2014"
country = "US"
---

## History

* 2013/10/29 - announced on bitcointalk [^1]
* 2014/01/07 - launched [^2]
* 2014/03/10 - hacked, announced shutdown [^3]

## Associated names

* Garrett Morris
* (mily6,fivemil https://bitcointalk.org/index.php?action=profile;u=78261)

* Openex, LLC

Past:

* Justin Gillett (justin7674 https://bitcointalk.org/index.php?action=profile;u=130551)
* (Krichwins https://bitcointalk.org/index.php?action=profile;u=140568)
* (Koontas )

## API


## Links

* https://github.com/r3wt/openex
* https://twitter.com/_OpenEx_
* http://whois.domaintools.com/openex.pw

## References

1. https://bitcointalk.org/index.php?topic=320975.0
2. https://bitcointalk.org/index.php?topic=402722.0
3. https://bitcointalk.org/index.php?topic=508674.0
