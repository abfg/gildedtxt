[exchange]
name = "Crypto X Change"
url = "http://www.cryptoxchange.com/"
founded = "2011"
country = "Australia"
[api]
streaming = 0
history = 0
trade = 0
---

## History

* 2011/11/10 - announced on bitcointalk [^1]
* 2012/11/19 - shutdown [^2,3]

## Associated names

* (cryptoxchange https://bitcointalk.org/index.php?action=profile;u=40345)

* Kenseycol PNY LTD

## API


## Links

* https://twitter.com/CryptoXChange
* http://whois.domaintools.com/cryptoxchange.com

## References

1. https://bitcointalk.org/index.php?topic=51457.0
2. https://bitcointalk.org/index.php?topic=76983.0
3. https://bitcointalk.org/index.php?topic=126552.0
