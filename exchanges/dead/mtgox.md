[exchange]
name = "Mt.Gox"
url = "https://mtgox.com/"
founded = "2010/07/18"
country = "Japan"
---

## History

In the before time, mtgox.com was going to be a Magic the Gathering trading cards exchange, but McCaleb decided to make a Bitcoin exchange instead.

Jed McCaleb sold Mt.Gox to Mark Karpeles in 2011. [^1]


## Associated names

* Jed McCaleb
* Mark Karpeles (MagicalTux)
* Gonzague Gay-Bouchery
* Dylan Davies-Bramley 
* Théo Da Costa
* John Duggan 


## API

* https://en.bitcoin.it/wiki/MtGox/API
* https://bitbucket.org/nitrous/mtgox-api/overview

## Links

* http://en.wikipedia.org/wiki/Mt.Gox
* http://www.tokyo-cci.or.jp/english/ibo/2353440.htm

## References

1. [Mtgox is changing owners](https://bitcointalk.org/index.php?topic=4187.msg60615)

