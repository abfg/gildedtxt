[exchange]
name = "BTC-TC"
url = "https://btct.co/"
founded = "2012"
country = "Seychelles"
---

## History

* 2012/08/19 - litecoinglobal announced on litecointalk [^4]
* 2012/11/07 - announced on bitcointalk [^1,2]
* 2013/10/31 - shutdown [^3]

## Associated names

* (burnside https://bitcointalk.org/index.php?action=profile;u=59012)
* (BTC-TradingCo https://bitcointalk.org/index.php?action=profile;u=68604)


## API


## Links

* http://http://whois.domaintools.com/btct.co
* http://http://whois.domaintools.com/litecoinglobal.com

## References

1. https://bitcointalk.org/index.php?topic=15672.msg1256789#msg1256789
2. https://bitcointalk.org/index.php?topic=125629.0
3. https://bitcointalk.org/index.php?topic=125629.msg3214807#msg3214807
4. https://litecointalk.org/index.php?topic=551.0

