[exchange]
name = "Vircurex"
url = "https://vircurex.com/"
founded = "2011"
country = "Germany"
---

## Notes

```
who is behind Vircurex: this is a German owned family business - read: yes a small organization at this point in time but as and when the volume grows, we'll adjust accordingly. We had the intention to incorporate in Germany. Now with the legal battles going on in France (between MT Gox and the French Authorities) about the treatment of Bitcoin and related bank transactions, we are rethinking the location; Hong Kong is surely one of them, China or other offshore locations are also being evaluated. The bank we are planning to use is HSBC (Hong Kong and/or China) [^4]
```

```
...unfortionately the service provider has then done and posted the credential in there helpdesk ticker, rather than the standard process of sending it to our email adress (witch has 2FA protection), also the security setup of allowing only our IP range to login to the management console was not working. It was an additional security feature the provider offered.

Consequensces  The loss of funds will be recovered out of the monthly dividends. Dividends will be used to purchase back the missing funds in the coming months. Depending on the trading volume development this is expected to take 9 to 12 months. [^5]
```

## History

* 2011/10/22 - launch announced on bitcointalk [^1]
* 2013/01/11 - Service compromised, funds stolen. [^2]
* 2013/02/02 - listed on Cryptostocks under the ticker VCX. [^3]
* 2014/03/24 - withdrawals frozens [^7]

## Associated names

* Kumala

## API

* https://vircurex.com/welcome/api?locale=en

trades: since id, 7 days history

## Links

* http://whois.domaintools.com/vircurex.com

## References

1. https://bitcointalk.org/index.php?topic=49383.0
2. https://bitcointalk.org/index.php?topic=135919.0
3. https://cryptostocks.com/securities/34
4. https://bitcointalk.org/index.php?topic=49382.msg595298#msg595298
5. https://bitcointalk.org/index.php?topic=226366.0
6. http://trilema.com/2013/why-you-cant-invest-with-cryptostocks/
7. https://vircurex.com/welcome/ann_reserved.html

