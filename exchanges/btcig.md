[exchange]
name = "BTCIG"
url = "https://www.btcig.com/"
founded = "2013"
country = "China"
---

## History

* 2013/12/13 - announced on bitcointalk [^1,2]

## Associated names

* (btcig https://bitcointalk.org/index.php?action=profile;u=189209)
* (monica https://bitcointalk.org/index.php?action=profile;u=190890)

## API

* http://www.btcig.com/api.php

## Links

* http://whois.domaintools.com/btcig.com

## References

1. https://bitcointalk.org/index.php?topic=369510
2. https://bitcointalk.org/index.php?topic=372128.0

