[exchange]
name = "Cryptostocks"
url = "https://cryptostocks.com/"
founded = "2012"
country = "Germany"
---

## History

* 2012/06/16 - announced on bitcointalk [^1]

A service from the developers of Vircurex.

## Associated names

* (Cryptostocks https://bitcointalk.org/index.php?action=profile;u=60207)
* (Kumala https://bitcointalk.org/index.php?action=profile;u=41776)

* Vircurex

## API

* https://cryptostocks.com/api

## Links

* http://whois.domaintools.com/cryptostocks.com

## References

1. https://bitcointalk.org/index.php?topic=88036.0
