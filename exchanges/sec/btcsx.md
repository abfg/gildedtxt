[exchange]
name = "btc.sx"
url = "https://btc.sx/"
founded = "2013"
country = "UK/Singapore"
---

## History

* 2013/04/27 - announced on bitcointalk [^1]
* 2014/02/25 - trading suspended due to Mt.Gox shutdown [^3]
* 2013/03/12 - trading resumes, integration with new exchanges [^4]
* 2014/04/01 - 500 BTC investment from Seedcoin [^5]

## Associated names

* Joe Lee
* Vincent Hoong
* James Turner
* George Samuel Samman

## API


## Links

* https://twitter.com/BTCsx
* http://whois.domaintools.com/btc.sx

## References

1. https://bitcointalk.org/index.php?topic=188735
2. http://www.hedgeweek.com/2014/01/22/196133/world%E2%80%99s-first-bitcoin-derivatives-platform-surpasses-usd35m-trades
3. http://www.coindesk.com/bitcoin-derivatives-market-btc-sx-suspends-trading-amid-turmoil-partner-mt-gox/
4. http://www.coindesk.com/btc-sx-resumes-trading-mt-gox-induced-freeze/
5. http://www.coindesk.com/seedcoin-gives-btc-sx-500-bitcoins-funding/
6. https://bitcointalk.org/index.php?topic=524429.0
