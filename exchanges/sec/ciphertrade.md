[exchange]
name = "CipherTrade"
url = "http://www.ciphertrade.com/"
founded = "2013"
country = "UK"
---

## History

* 2013/11/30 - announced on bitcointalk [^1]

## Associated names

* Kate Craig-Wood

## API


## Links

* https://twitter.com/Ciphertrade
* http://whois.domaintools.com/ciphertrade.com

## References

1. https://bitcointalk.org/index.php?topic=353594.0

