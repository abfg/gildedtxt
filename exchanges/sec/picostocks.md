[exchange]
name = "PicoStocks"
url = "https://picostocks.com/"
founded = "2012"
country = "Poland"
---

## Notes

```
The platform is operated by Picostocks Incorporated, Trust Company Complex, Ajeltake Road, Ajeltake Island, Majuro, Marshall Islands MH96960.

The IPO office and the WWW service is operated by BioInfoBank, Sw. Marcin 80/82 lok. 355, 61-809 Poznan, Poland.

E-mail: contact@picostocks.com Phone: +48 618653520 Fax: +48 223001563
```

## History

* 2012/12/24 - announced on bitcointalk [^1]
* 2013/11/29 - funds stolen [^1]

## Associated names

* Maciej Kazmierczyk
* Leszek Rychlewski

* Dave Carlson

* Picostocks Incorporated, Marshall Islands
* BioInfoBank, Poland

## API


## Links

* http://whois.domaintools.com/picostocks.com

## References

1. https://bitcointalk.org/index.php?topic=133147.0
2. https://bitcointalk.org/index.php?topic=133147.msg3769061#msg3769061
3. https://bitcointalk.org/index.php?topic=140366.0
4. http://www.heise.de/ct/meldung/Bitcoin-Mining-in-Eigenbau-Rechenzentren-mit-Billigstrom-2156192.html

