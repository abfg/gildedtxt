[exchange]
name = "Havelock"
url = "https://www.havelockinvestments.com/"
founded = "2013"
country = "Panama"
---

## History

* 2012/06/11 - announced on bitcointalk [^1]
* 2013/11/01 - acquired by The Panama Fund [^2]

## Associated names

* Marvin Tan
* (havelock https://bitcointalk.org/index.php?action=profile;u=64492)

* James Grant
* (topace http://bitcoin-otc.com/newsite/viewratingdetail.php?nick=topace)


* The Panama Fund, S.A

## API

* https://www.havelockinvestments.com/apidoc.php

## Links

* https://twitter.com/Havelock_Inv
* http://whois.domaintools.com/havelockinvestments.com

## References

1. https://bitcointalk.org/index.php?topic=87002.0
2. https://bitcointalk.org/index.php?topic=323077.0
3. https://bitcointalk.org/index.php?topic=135035.msg3969507#msg3969507
4. http://www.coindesk.com/neo-bee-share-price-plunges-trading-resumes/
