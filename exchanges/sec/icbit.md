[exchange]
name = "ICBit"
url = "https://icbit.se/"
founded = "2011"
country = "Sweden"
---

## History

* 2011/11/03 - announced on bitcointalk [^1]

## Associated names

* (Fireball https://bitcointalk.org/index.php?action=profile;u=21659)
* ([Tycho] https://bitcointalk.org/index.php?action=profile;u=7603)

## API

* https://icbit.se/api

## Links

* https://twitter.com/icbit_se
* http://whois.domaintools.com/icbit.se

## References

1. https://bitcointalk.org/index.php?topic=50817.0
2. https://bitcointalk.org/index.php?topic=246845.0
3. https://bitcointalk.org/index.php?topic=125376.0
