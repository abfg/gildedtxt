[exchange]
name = "Cryptonit"
url = "https://cryptonit.net/"
founded = "2013"
country = "UK"
---

## History

* 2014/02/21 - company formation announced [^1]

## Associated names

* Andrei Yanukovich
* Daniel?
* Brandon?
* David Albert Sheridan

* Cryptonit Solutions Ltd

## API

* https://cryptonit.net/faq

## Links

* http://whois.domaintools.com/cryptonit.net

## References

1. https://cryptonit.net/content/introducing-cryptonit-solutions-ltd
