[exchange]
name = "Poloniex"
url = "https://poloniex.com/"
founded = "2013"
country = "UK"
---

## History

* 2014/01/18 - announced on bitcointalk [^1]
* 2014/03/04 - hacked, 76.69 BTC stolen [^2,3]

## Associated names

* (busoni https://bitcointalk.org/index.php?action=profile;u=139578)

## API

* https://poloniex.com/api

trades: no since id, last 200 trades

## Links

* https://twitter.com/Poloniex
* http://whois.domaintools.com/poloniex.com

## References

1. https://bitcointalk.org/index.php?topic=420836.0
2. https://bitcointalk.org/index.php?topic=499580
3. http://arstechnica.com/security/2014/03/yet-another-exchange-hacked-poloniex-loses-around-50000-in-bitcoin/

