[exchange]
name = "Kapiton"
url = "https://kapiton.se/"
founded = "2013"
country = "Sweden"
---

## History


## Associated names

* Sebastian Manitski
* Mike Buercart (https://bitcointalk.org/index.php?action=profile;u=78643)

## API

* https://kapiton.se/api

## Links

* http://whois.domaintools.com/kapiton.se

## References

1. http://www.techienews.co.uk/974868/swedens-largest-bitcoin-site-kapiton-se-mia-users-complain-missing-funds/
2. http://www.reddit.com/r/Bitcoin/comments/1uxfp7/kapitonse_is_a_scam_its_owner_sebastian_manitski/
3. http://www.metro.se/teknik/owner-accused-for-bitcoin-scam-i-ve-been-depressed/EVHnaq!X7N4W6nUf1vrg/
4. https://bitcointalk.org/index.php?topic=423631.0
