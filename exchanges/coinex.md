[exchange]
name = "Coinex"
url = "https://coinex.pw/"
founded = "2013"
country = ""
---

## History

* 2013/07/31 - launch announced on bitcointalk [^1]
* 2014/01/26 - listed on Cryptostocks [^2]
* 2014/03/18 - hacked [^4,5,6]

## Associated names

* captainfuture (https://bitcointalk.org/index.php?action=profile;u=120370)
* erundook (https://bitcointalk.org/index.php?action=profile;u=114495)

## API

* https://gist.github.com/erundook/8377222

trades: no since id, no history

## Links

* https://twitter.com/CoinexPW
* http://whois.domaintools.com/coinex.pw

## References

1. https://bitcointalk.org/index.php?topic=265277.0
2. https://cryptostocks.com/securities/79
3. https://bitcointalk.org/index.php?topic=436746
4. https://bitcointalk.org/index.php?topic=265277.msg5769484#msg5769484
5. http://www.coindesk.com/coinex-pw-hacked-will-cover-losses/
6. https://bitcointalk.org/index.php?topic=265277.msg5955017#msg5955017
