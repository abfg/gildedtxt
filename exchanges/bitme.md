[exchange]
name = "BitMe"
url = "https://bitme.com/"
founded = "2012"
country = "US"
---

## History

* 2012/05/12 - announced on bitcointalk [^1]
* 2013/03/20 - shutdown [^2,3]
* 2014 - acquired by Vaurum?

## Associated names

* Sean Levine (freewil https://bitcointalk.org/index.php?action=profile;u=56571)

## API

* http://bitme.github.io/rest/

## Links

* https://twitter.com/bitme
* http://whois.domaintools.com/bitme.com

## References

1. https://bitcointalk.org/index.php?topic=81433.0
2. https://bitcointalk.org/index.php?topic=155546.0
3. https://bitcointalk.org/index.php?topic=155864.0
