[exchange]
name = "Crypto-Change"
url = "https://www.crypto-change.com/"
founded = "2013"
country = "Hong Kong"
---

## History

* 2013/08/11 - announced on bitcointalk [^1]


## Associated names

* Mike London (nullzsec,Crypto-Change.com https://bitcointalk.org/index.php?action=profile;u=143895)

## API

* https://www.crypto-change.com/#API

## Links

* http://whois.domaintools.com/crypto-change.com

## References

1. https://bitcointalk.org/index.php?topic=271812.0
