[exchange]
name = "CryptOTC"
url = "http://www.cryptotc.us/"
founded = "2014"
country = "-"
---

## History

* 2014/01/14 - announced on bitcointalk [^1]

## Associated names

* (fxmulder https://bitcointalk.org/index.php?action=profile;u=115426)

## API


## Links

* http://whois.domaintools.com/cryptotc.us

## References

1. https://bitcointalk.org/index.php?topic=414581.0

