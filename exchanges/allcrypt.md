[exchange]
name = "AllCrypt"
url = "https://www.allcrypt.com/"
founded = "2014"
country = "US
---

## History

* 2014/01/29 - announced on bitcointalk [^1]
* 2014/03/01 - launch [^2]
* 2014/03/21 - hacked [^3,4,5]

## Associated names

* (allcrypt https://bitcointalk.org/index.php?action=profile;u=232503)

## API

* https://allcrypt.zendesk.com/hc/en-us/articles/201485504-Public-Market-API-documentation

## Links

* https://twitter.com/all_crypt
* https://www.facebook.com/allcrypt
* http://whois.domaintools.com/allcrypt.com

## References

1. https://bitcointalk.org/index.php?topic=438727
2. https://bitcointalk.org/index.php?topic=494842
3. https://www.allcrypt.com/blog/2014/03/hack-update-1/
4. https://www.allcrypt.com/blog/2014/03/hack-update-2-what-the-hell-actually-happened/
5. https://www.allcrypt.com/blog/2014/03/the-hack-the-resolution/
