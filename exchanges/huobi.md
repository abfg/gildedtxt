[exchange]
name = "Huobi"
url = "https://www.huobi.com/"
founded = "2013"
country = "China"
---

## History


## Associated names

* Leon Li / Li Lin

## API

* http://www.huobi.com/help/index.php?a=api_help
* http://www.huobi.com/help/index.php?a=market_help

## Links

* https://bitcointalk.org/index.php?action=profile;u=141508
* http://whois.domaintools.com/huobi.com

## References

1. http://forexmagnates.com/exclusive-interview-ceo-of-largest-bitcoin-exchange-in-the-world-huobi-responds-to-mt-gox-situation/
2. http://www.reddit.com/r/Bitcoin/comments/1usf77/huobi_playing_a_very_dangerous_game_thoughts_from/
3. http://www.coindesk.com/chinese-exchange-huobi-commence-trading-litecoin/
4. http://www.coindesk.com/chinese-bitcoin-exchange-huobi-stop-voucher-deposits-amid-uncertainty/
