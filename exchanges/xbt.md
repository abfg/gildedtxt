[exchange]
name = "X-BT"
url = "https://x-bt.com/"
founded = "2013"
country = "-"
---

## History

* 2013/04/26 - announced on bitcointalk [^1]
* 2013/09/14 - relaunch [^2]

## Associated names

* (Apocalyptic https://bitcointalk.org/index.php?action=profile;u=86111)
* (Lena-XBTC https://bitcointalk.org/index.php?action=profile;u=109025)

## API

* https://x-bt.com/api

## Links

* http://whois.domaintools.com/x-bt.com

## References

1. https://bitcointalk.org/index.php?topic=188304.0
2. https://bitcointalk.org/index.php?topic=188304.msg3154688#msg3154688
