[exchange]
name = "Coinsetter"
url = "https://www.coinsetter.com/"
founded = "2013"
country = "US"
---

## History

* 2013/02/06 - announced on bitcointalk [^1]
* 2013/04/09 - raised $500k [^2]
* 2013/11/13 - open for trading [^4]

Has access to Bitstamp's book. [^3]

Around $1 million in VC funding. [^5]

## Associated names

* Jaron Lukasiewicz
* Marshall Swatt
* Yo Sub Kwon
* Johann Botha 

* Coinsetter Inc.
* Coinsetter International Canada Ltd

## API

* https://www.coinsetter.com/api


## Links

* http://whois.domaintools.com/coinsetter.com

## References

1. https://bitcointalk.org/index.php?topic=141638.0
2. http://techcrunch.com/2013/04/09/coinsetter-lands-500k-from-secondmarket-founder-others-to-help-bring-leverage-shorting-to-bitcoin-trade/
3. http://coinsetter.uservoice.com/knowledgebase/articles/234946-what-exchanges-does-coinsetter-aggregate-
4. http://blog.coinsetter.com/2013/11/13/coinsetter-releases-its-high-performance-bitcoin-trading-platform-to-the-public/
5. http://www.crunchbase.com/company/coinsetter
