[exchange]
name = "Bitfinex"
url = "https://bitfinex.com/"
founded = "2013"
country = "Hong Kong"
---

## History

* 2012/10/22 - beta announced [^1]
* 2013/06/08 - announced on bitcointalk [^2]

## Associated names

* Raphael Nicolle (unclescrooge https://bitcointalk.org/index.php?action=profile;u=4295)
* Giancarlo Devasini (urwhatuknow https://bitcointalk.org/index.php?action=profile;u=70409)
* Jean-Louis Van Der Velde (http://hk.linkedin.com/pub/j-l-van-der-velde/6/24b/26a)

* Justin Burns

## API

* https://www.bitfinex.com/pages/api

trades: no since id, no trade ids

## Links

* https://twitter.com/bitfinex
* http://whois.domaintools.com/bitfinex.com

## References

1. https://bitcointalk.org/index.php?topic=119745.0
2. https://bitcointalk.org/index.php?topic=229438.0
3. https://bitcointalk.org/index.php?topic=224745.0
