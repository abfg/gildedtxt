[exchange]
name = "CoinMarket"
url = "https://www.coinmarket.io/"
founded = "2014"
country = "EU"
---

## History

* 2014/02/07 - announced on bitcointalk [^1]

## Associated names

* (coinmarket.io https://bitcointalk.org/index.php?action=profile;u=221995)

## API


## Links

* https://twitter.com/CoinMarketio
* http://whois.domaintools.com/coinmarket.io

## References

1. https://bitcointalk.org/index.php?topic=454186

