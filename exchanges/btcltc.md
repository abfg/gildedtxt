[exchange]
name = "BTCLTC"
url = "http://www.btcltc.com/"
founded = "2013/06"
country = "China"
---

## History


## Associated names

* Ningbo High Valley Network Technology Co., Ltd. [^1]

## API

* http://www.btcltc.com/home/index/api

trades: since id, full history

## Links

* http://whois.domaintools.com/btcltc.com

## References

1. http://www.btcltc.com/home/index/help#about
