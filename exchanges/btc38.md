[exchange]
name = "BTC38"
url = "http://www.btc38.com/"
founded = "2013"
country = "China"
---

## History

* 2014/03/05 - English website announced [^21]

## Associated names

* (btc38 https://bitcointalk.org/index.php?action=profile;u=262888)

## API

* http://www.btc38.com/general/789.html

## Links

* http://whois.domaintools.com/btc38.com

## References

1. https://bitcointalk.org/index.php?topic=501464.0

