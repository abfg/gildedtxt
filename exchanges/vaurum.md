[exchange]
name = "Vaurum"
url = "https://www.vaurum.com/"
founded = "2013"
country = "US"
---

## History

bitme.com is wholly owned and operated by Vaurum.

## Associated names

* Avish Bhama
* Sean Lavine
* Byron Gibson

## API


## Links

* https://twitter.com/vaurum
* http://whois.domaintools.com/vaurum.com

## References

1. http://www.reddit.com/r/Bitcoin/comments/1mwye6/vaurum_we_make_it_easy_to_buy_trade_and_store/
