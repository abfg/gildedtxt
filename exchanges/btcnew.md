[exchange]
name = "BTCnew"
url = "http://www.btcnew.net/"
founded = "2013"
country = "China"
---

## History


## Associated names

* BTCNEW Technology Company Limited [^1]

## API

* http://www.btcnew.net/btc/api

trades: since id, full history, history broken for LTC

## Links

* http://whois.domaintools.com/btcnew.net

## References

1. https://www.btcnew.net/btc/aboutus
