[exchange]
name = "CoinedUp"
url = "https://coinedup.com/"
founded = "2013"
country = "US"
---

## History

* 2013/07/29 - introduction on bitcointalk [^1]

## Associated names

* Pete (https://bitcointalk.org/index.php?action=profile;u=120898)

## API

* https://api.coinedup.com/

trades: since id, full history

## Links

* https://twitter.com/CoinedUp
* http://whois.domaintools.com/chbtc.com

## References

1. https://bitcointalk.org/index.php?topic=264327.msg2826999#msg2826999
