[exchange]
name = "Bitstamp"
url = "https://bitstamp.net/"
founded = "2011"
country = "Slovenia"
---

## History

* 2012/07/25 - Bitstamp Ltd. incorporated in UK

## Associated names

* Nejc Kodrič (https://bitcointalk.org/index.php?action=profile;u=38966)
* Damijan Merlak

## API

* https://www.bitstamp.net/api/


## Links

* http://en.wikipedia.org/wiki/Bitstamp
* https://twitter.com/Bitstamp
* https://www.facebook.com/Bitstamp
* http://whois.domaintools.com/bitstamp.net

## References


