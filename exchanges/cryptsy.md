[exchange]
name = "Cryptsy"
url = "https://www.cryptsy.com/"
founded = "2013"
country = "US"
---

## History

* 2013/05/20 - open for trading [^1]
* 2013/10/23 - listed on Cryptostocks [^2]

## Associated names

* Paul Vernon
* Bryant Ibana
* Stacy Taylor
* BitJohn (https://bitcointalk.org/index.php?action=profile;u=110699)
* Bradford Newton

* Project Investors, Inc.

## API

* https://www.cryptsy.com/pages/api

trades: no since id, last 1000 trades

## Links

* https://twitter.com/cryptsy
* http://whois.domaintools.com/cryptsy.com

## References

1. https://www.cryptsy.com/pages/about
2. https://cryptostocks.com/securities/57
3. https://bitcointalk.org/index.php?topic=477144.0
