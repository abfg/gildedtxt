[exchange]
name = "BTC China"
url = "https://btcchina.com/"
founded = "2011/06"
country = "China"
---

## History

* 2011/06 - founded by Linked Yang
* 2013/04 - Bobby Lee becomes CEO
* 2013/11/08 - closed a $5 million Series A from institutional investors Lightspeed China Partners and Lightspeed Venture Partners. [^1]

## Associated names

* Linke Yang (founder)
* Bobby Lee
* Xiaoyu Huang

## API

* http://btcchina.org/api-market-data-documentation-en
* http://btcchina.org/api-trade-documentation-en

trades: since id, full history

## Links

* http://en.wikipedia.org/wiki/BTC_China
* http://whois.domaintools.com/btcchina.com

## References

1. http://techcrunch.com/2013/11/18/btc-china-series-a/
2. http://bitcoinmagazine.com/8055/8btc-interview-with-btcchina/

