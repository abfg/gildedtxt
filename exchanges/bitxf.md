[exchange]
name = "BitXF"
url = "https://en.bitxf.com/"
founded = "2013"
country = "China"
---

## History

* 2013/04/19 - announced on bitcointalk [^1]
* 2013/05/02 - first tweet [^2]


## Associated names

* (bitxf https://bitcointalk.org/index.php?action=profile;u=88004)

## API

* https://gist.github.com/itarkus/7314884

## Links

* https://twitter.com/bitxf
* http://whois.domaintools.com/bitxf.com

## References

1. https://bitcointalk.org/index.php?topic=180299
2. https://twitter.com/bitxf/status/329831679998951425
