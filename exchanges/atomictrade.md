[exchange]
name = "Atomic Trade"
url = "https://www.atomic-trade.com/"
founded = "2013"
country = "US"
---

## History

* 2014/01/02 - announced on bitcointalk [^1]

## Associated names

* Allen Byron Penner

* Atomic Trade, LLC

## API

* https://www.atomic-trade.com/MarketAPI

## Links

* https://twitter.com/AtomicTradeLLC
* http://whois.domaintools.com/atomic-trade.com

## References

1. https://bitcointalk.org/index.php?topic=395192

