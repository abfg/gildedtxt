[exchange]
name = "bitNZ"
url = "https://bitnz.com/"
founded = "2011"
country = "New Zealand"
---

## History

* 2011/09/12 - announced on bitcointalk [^1]

## Associated names

* (djpnewton https://bitcointalk.org/index.php?action=profile;u=38814)

* djpsoft

## API


## Links

* http://whois.domaintools.com/bitnz.com

## References

1. https://bitcointalk.org/index.php?topic=43609.0

