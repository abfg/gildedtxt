[exchange]
name = "CHBTC"
url = "https://bitkonan.com/"
founded = "2013"
country = "Croatia"
[api]
streaming = 0
history = 0
trade = 0
---

## History

* 2013/07/02 - announced on bitcointalk [^1]

## Associated names

* Sime Bakic (fx1Trader https://bitcointalk.org/index.php?action=profile;u=32438)

* Res Rei j.d.o.o.

## API

* https://bitkonan.com/info/api


## Links

* http://whois.domaintools.com/bitkonan.com

## References

1. https://bitcointalk.org/index.php?topic=248471.0
