[exchange]
name = "CryptoRush"
url = "https://cryptorush.in/"
founded = "2014"
country = "EU"
---

## History

* 2014/01/25 - announced on bitcointalk [^1]
* 2014/03/11 - hacked, 950 BTC and 2500 LTC stolen [^4]
* 2014/03/26 - insider info leaked [^4]

## Associated names

* Chris

* Kristian Thomsen (linkandzelda https://bitcointalk.org/index.php?action=profile;u=180394)
* Robert Keith Cristopher, Jr (Devianttwo https://bitcointalk.org/index.php?action=profile;u=112617)
* (CryptoRush https://bitcointalk.org/index.php?action=profile;u=224348)
* Matt Sartain

## API


## Links

* https://twitter.com/TheCryptoRush
* http://whois.domaintools.com/cryptorush.in

## References

1. https://bitcointalk.org/index.php?topic=431099.0
2. https://bitcointalk.org/index.php?topic=431099.msg5376469#msg5376469
3. http://trilema.com/2014/robert-keith-cristopher-jr-matt-sartain-kristian-thomson-cryptorush-much-comedy-lulzgoldmine/
4. http://pastebin.com/eLkPxLWi
5. http://www.cryptocoinsnews.com/news/cryptorush-mt-gox-alt-coins/2014/04/02
