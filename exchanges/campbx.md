[exchange]
name = "CampBX"
url = "https://campbx.com/"
founded = "2011"
country = "US"
---

## History

* 2011/06/11 - beta announced on bitcointalk [^1]
* 2011/06/05 - open for trading [^2]

## Associated names

* Keyur Mithawala

* Bulbul Investments LLC 

## API

* https://campbx.com/api.php


## Links

* https://twitter.com/CampBX
* https://www.facebook.com/CampBX
* http://whois.domaintools.com/campbx.com

## References

1. https://bitcointalk.org/index.php?topic=20777.0
2. https://bitcointalk.org/index.php?topic=26202.0
