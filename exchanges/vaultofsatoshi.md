[exchange]
name = "Vault of Satoshi"
url = "https://www.vaultofsatoshi.com/"
founded = "2013"
country = "Canada"
---

## History

* changed name from BTCTO.com to Bank of Satoshi [^3]
* changed name from Bank of Satoshi after warning [^2]

## Associated names

* Michael Curry
* Ryan van Barneveld

* Adam (AdamSC1 https://bitcointalk.org/index.php?action=profile;u=293253)
* Larry Philps
* Meg Curry
* Andrea Clendening
* Angelo Rodrigues
* Stacy Pacheco
* Noreen Borne
* Ken Vahl
* Nichole McMullen
* Tyama Camara
* Ellyse Mundy
* Ashley Mallon

* Global CryptoCurrency Solutions Incorporated [^1]

## API

* https://www.vaultofsatoshi.com/api

## Links

* https://twitter.com/vaultofsatoshi
* https://www.facebook.com/vaultofsatoshi
* http://whois.domaintools.com/vaultofsatoshi.com

## References

1. https://www.vaultofsatoshi.com/about
2. http://mrwatchlist.com/2013/08/07/osfi-warning-notice-for-bank-of-satoshi/
3. http://www.coindesk.com/vault-satoshi-expands-canadian-bitcoin-exchange-market/
4. http://www.reddit.com/r/Bitcoin/comments/1oqdoo/vault_of_satoshi_new_north_american_exchange_is/
5. http://www.brantfordexpositor.ca/2014/01/31/bitcoin-exchange-launches-in-brantford
6. http://bitcoinmagazine.com/11667/canadian-exchange-approved-by-fintrac/

