[exchange]
name = "MintPal"
url = "https://www.mintpal.com/"
founded = "2014"
country = "UK"
---

## History

* 2014/02/05 - announced on bitcointalk [^1]

## Associated names

* (MintPal-Jason https://bitcointalk.org/index.php?action=profile;u=239463)
* (MintPal-Jay https://bitcointalk.org/index.php?action=profile;u=238724)

## API

* https://www.mintpal.com/api
* https://www.mintpal.com/api/v2

## Links

* https://twitter.com/MintPalExchange
* http://whois.domaintools.com/mintpal.com

## References

1. https://bitcointalk.org/index.php?topic=442876.msg4952331#msg4952331
