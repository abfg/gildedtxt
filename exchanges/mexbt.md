[exchange]
name = "MEXBT"
url = "http://www.mexbt.com/"
founded = "2013"
country = "Mexico"
---

## History

* 2014/04/01 - 250 BTC investment from Seedcoin [^1]

## Associated names

* Gabriel Miron (gabrielmiron https://bitcointalk.org/index.php?action=profile;u=150689)

## API



## Links

* https://twitter.com/mexbt
* http://whois.domaintools.com/mexbt.com

## References

1. http://www.coindesk.com/seedcoin-gives-btc-sx-500-bitcoins-funding/
